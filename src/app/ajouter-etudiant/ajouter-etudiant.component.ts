import { Component, OnInit } from '@angular/core';
import { Etudiant } from '../../etudiant';
import { EtudiantService } from '../etudiant.service';

@Component({
  selector: 'app-ajouter-etudiant',
  templateUrl: './ajouter-etudiant.component.html',
  styleUrls: ['./ajouter-etudiant.component.css']
})
export class AjouterEtudiantComponent implements OnInit {

  etudiantRecuperer:Etudiant= new Etudiant();
  listetud:Etudiant[]= [];

  constructor(private etudiantService:EtudiantService) { }

  ngOnInit() {
  }


  ajouteretudiant(etudiantRecuperer:Etudiant):void{

    //etudiantRecuperer=etudiantRecuperer.lis

    if(!etudiantRecuperer){console.log("jai rien recu come etudiant ")}
    this.etudiantService.ajouteretudiant(etudiantRecuperer)
    .subscribe(etudiantRecuperer => {
      this.listetud.push(etudiantRecuperer);
    })
  }


}
