import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupprimerProfesseurComponent } from './supprimer-professeur.component';

describe('SupprimerProfesseurComponent', () => {
  let component: SupprimerProfesseurComponent;
  let fixture: ComponentFixture<SupprimerProfesseurComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupprimerProfesseurComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupprimerProfesseurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
