import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrouverEtudiantComponent } from './trouver-etudiant.component';

describe('TrouverEtudiantComponent', () => {
  let component: TrouverEtudiantComponent;
  let fixture: ComponentFixture<TrouverEtudiantComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrouverEtudiantComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrouverEtudiantComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
