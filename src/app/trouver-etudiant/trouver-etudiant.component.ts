import { Component, OnInit } from '@angular/core';
import { Etudiant } from '../../etudiant';
import { Observable, Subject } from 'rxjs';
import { EtudiantService } from '../etudiant.service';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { Http } from '@angular/http';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: 'app-trouver-etudiant',
  templateUrl: './trouver-etudiant.component.html',
  styleUrls: ['./trouver-etudiant.component.css']
})

export class TrouverEtudiantComponent implements OnInit {

  id:number;
  etudiantrecuprer:any;

  constructor(public http:Http,
              private etudiantservice:EtudiantService,
             // private _router: Router,
              private _route:ActivatedRoute) { }

  ngOnInit() {
    //let id = +this._route.snapshot.paramMap.get('id');
}

public rechercheEtudiant(){
this.etudiantservice.rechercheEtudiant(this.id)
.subscribe(data=>{
     this.etudiantrecuprer=data;
   },error=>{
   console.log("error");
   })
  }
}