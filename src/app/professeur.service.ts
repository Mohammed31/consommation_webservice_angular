import { Injectable } from '@angular/core';
import { HttpClient,HttpHeaders} from '@angular/common/http';
import { of, Observable } from 'rxjs';
import { Professeur } from '../professeur';
import { map, catchError,tap } from 'rxjs/operators';
import 'rxjs/add/operator/map';

const httpOptions={
  headers: new HttpHeaders({ 'Content-Type':'application/JSON' })
};

@Injectable({
  providedIn: 'root'
})


export class ProfesseurService {

  private APIUrl = 'http://localhost:8080/professeurs/delete';
  private APIUrlsave = 'http://localhost:8080/professeurs/save';

  
  constructor(public http:HttpClient) { }

  getlistProf(){
  return this.http.get("http://localhost:8080/professeurs/list")
  .map(resp=>resp);
}


private handleError<T> (operation = 'operation', result?: T) {
  return (error: any): Observable<T> => {
    console.error(error); 
    this.log('loulou');
    return of(result as T);
  };
}
public deleteProf(profs:Professeur | number): Observable<Professeur>{
    
  const id = typeof profs === 'number'? profs : profs.id;
  const url = `${this.APIUrl}/${id}`;

  return this.http.delete<Professeur>(url,httpOptions).pipe(
    tap(_=>this.log('loulou')),
    catchError(this.handleError<Professeur>('deletProf'))
  );
}

private log(mesg:string) {
  console.log("ca marche ");
}

public rechercheProfesseur(id:number):Observable<Professeur>{
  console.log("je suis dans service  "+id);
  return this.getlistProf().pipe(
    map((listetudiants:Professeur[]) => listetudiants.find(prof=>prof.id===id))
  );
}

ajouterProfesseur (professeur:Professeur):Observable<Professeur>{
  return this.http.post<Professeur>(this.APIUrlsave,professeur,httpOptions).pipe(
  tap((professeur:Professeur) => this.log(`ajouter professeur w/ id=${professeur.id}`)),
    catchError(this.handleError<Professeur>('ajouterProfesseur'))
  );
}


}