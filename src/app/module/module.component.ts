import { Component, OnInit } from '@angular/core';
import { ModulesService } from '../modules.service';
import { Modules } from '../modules';
import { ActivatedRoute } from '@angular/router';
import { Http } from '@angular/http';

@Component({
  selector: 'app-module',
  templateUrl: './module.component.html',
  styleUrls: ['./module.component.css']
})
export class ModuleComponent implements OnInit {

moduleRecuperer:Modules = new Modules();
listeModule:Modules[]=[];
pagelistModules:any;
id:number;
Modulerecuprer:any;

  constructor(private _route:ActivatedRoute,public http:Http,private modulesService:ModulesService) { }

  ngOnInit() {
    this.modulesService.getlistModules()
    .subscribe(data=>{
      this.pagelistModules=data;
    },error=>{
      console.log("error");
    })
  }

  ajouterModule(moduleRecuperer:Modules):void{

    if(!moduleRecuperer){console.log("jai rien recu come module ")}
    this.modulesService.ajouterModule(moduleRecuperer)
    .subscribe(moduleRecuperer => {
      this.listeModule.push(moduleRecuperer);
    })
  }

  public deletModule(mod:Modules):void{
    this.listeModule = this.listeModule.filter(m=>m !==mod);
    this.modulesService.deletModule(mod).subscribe();
  }

  public rechercheModule(){
    this.modulesService.rechercheModule(this.id)
    .subscribe(data=>{
         this.Modulerecuprer=data;
       },error=>{
       console.log("error");
       })
      }



}
