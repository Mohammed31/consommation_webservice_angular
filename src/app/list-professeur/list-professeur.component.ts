import { Component, OnInit } from '@angular/core';
import { Http } from '@angular/http';
import { ProfesseurService } from '../professeur.service';
import { Professeur } from '../../professeur';

@Component({
  selector: 'app-list-professeur',
  templateUrl: './list-professeur.component.html',
  styleUrls: ['./list-professeur.component.css']
})
export class ListProfesseurComponent implements OnInit {

pagelistprof:any;
 particlesJS: any;

  constructor(public http:Http,public professeurService:ProfesseurService) { }

  
  ngOnInit() {
    this.professeurService.getlistProf()
    .subscribe(data=>{
      this.pagelistprof=data;
    },error=>{
      console.log("error");
    })

   this.particlesJS.load('particles-js', 'particles.json', null);
  }

  public deleteProf(prof:Professeur):void{
    this.pagelistprof=this.pagelistprof.filter(p=>p !==prof);
    this.professeurService.deleteProf(prof).subscribe();
  }

}
