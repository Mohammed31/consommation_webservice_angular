import { Component, OnInit } from '@angular/core';
import { Professeur } from '../../professeur';
import { Http } from '@angular/http';
import { ProfesseurService } from '../professeur.service';

@Component({
  selector: 'app-trouver-professeur',
  templateUrl: './trouver-professeur.component.html',
  styleUrls: ['./trouver-professeur.component.css']
})
export class TrouverProfesseurComponent implements OnInit {

  id:number;
  professeurRecuperer:Professeur;
  constructor(public http:Http,
              private professeurService:ProfesseurService) { }

  ngOnInit() {
  }

  private rechercheProfesseur(){
    this.professeurService.rechercheProfesseur(this.id)
    .subscribe(data=>{
      this.professeurRecuperer=data;
    },error=>{
      console.log("reur recup prof");
        })
  }

}
