import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TrouverProfesseurComponent } from './trouver-professeur.component';

describe('TrouverProfesseurComponent', () => {
  let component: TrouverProfesseurComponent;
  let fixture: ComponentFixture<TrouverProfesseurComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TrouverProfesseurComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TrouverProfesseurComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
