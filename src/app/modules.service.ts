import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable,of } from 'rxjs';
import { Modules } from './modules';
import { map, tap, catchError } from 'rxjs/operators';

const httpOptions={
  headers: new HttpHeaders({ 'Content-Type':'application/JSON' })
};

@Injectable({
  providedIn: 'root'
})

export class ModulesService {

  private APIUrl = 'http://localhost:8080/modules/delete';
  private APIUrlsave = 'http://localhost:8080/modules/save';

  constructor(public http: HttpClient) { }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error); 
      this.log('hela');
      return of(result as T);
    };
  }
  
  private log(mesg:string) {
    console.log("ca marche ");
  }

  getlistModules(){
    return this.http.get("http://localhost:8080/modules/list")
    .map(resp => resp);
   
  }

  public rechercheModule(id:number):Observable<Modules>{
    console.log("je suis dans service  "+id);
    return this.getlistModules().pipe(
    map((listeModule:Modules[]) => listeModule.find(mod=>mod.id===id))
    );
    
  }
  
  public deletModule(modul:Modules | number): Observable<Modules>{
  
    const id = typeof modul === 'number'? modul : modul.id;
    const url = `${this.APIUrl}/${id}`;
    return this.http.delete<Modules>(url,httpOptions).pipe(
      tap(_=>this.log('delete and gone')),
      catchError(this.handleError<Modules>('deletModule'))
    );
  }
  
  ajouterModule (modul:Modules):Observable<Modules>{
    return this.http.post<Modules>(this.APIUrlsave,modul,httpOptions).pipe(
    tap((modul:Modules) => this.log(`ajouter module w/ id=${modul.id}`)),
      catchError(this.handleError<Modules>('ajouterModule'))
    );
  }
}
