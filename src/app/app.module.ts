import { AlertModule } from 'ngx-bootstrap';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
 
import { AppComponent } from './app.component';
import { Route } from '@angular/compiler/src/core';
import { AjouterEtudiantComponent } from './ajouter-etudiant/ajouter-etudiant.component';
import { SupprimerEtudiantComponent } from './supprimer-etudiant/supprimer-etudiant.component';

import { AjouterProfesseurComponent } from './ajouter-professeur/ajouter-professeur.component';
import { ListProfesseurComponent } from './list-professeur/list-professeur.component';
import { ListeEtudiantComponent } from './liste-etudiant/liste-etudiant.component';
import { SupprimerProfesseurComponent } from './supprimer-professeur/supprimer-professeur.component';
import { TrouverEtudiantComponent } from './trouver-etudiant/trouver-etudiant.component';
import { TrouverProfesseurComponent } from './trouver-professeur/trouver-professeur.component';
import { HttpModule } from '@angular/http';
import { EtudiantService } from './etudiant.service';
import { ProfesseurService } from './professeur.service';
import { FormsModule } from '@angular/forms';
import { ModuleComponent } from './module/module.component';

const appRoutes:Routes=[
  {path:'listeEtu',component:ListeEtudiantComponent},
  {path:'ajouterEtu',component:AjouterEtudiantComponent},
  {path:'supprimerEtu',component:SupprimerEtudiantComponent},
  {path:'chercherEtu',component:TrouverEtudiantComponent},
  {path:'ajouterProf',component:AjouterProfesseurComponent},
  {path:'supprimerProf',component:SupprimerProfesseurComponent},
  {path:'chercherProf',component:TrouverProfesseurComponent},
  {path:'listeProf',component:ListProfesseurComponent},
  {path:'modules',component:ModuleComponent},

{path:'',
redirectTo:'/listeEtu',
pathMatch:'full'
}
];

@NgModule({
  declarations: [
    AppComponent,
    AjouterEtudiantComponent,
    SupprimerEtudiantComponent,
    AjouterProfesseurComponent,
    ListProfesseurComponent,
    ListeEtudiantComponent,
    SupprimerProfesseurComponent,
    TrouverEtudiantComponent,
    TrouverProfesseurComponent,
    ModuleComponent

  ],

  imports: [
    BrowserModule ,RouterModule.forRoot(appRoutes),HttpModule,HttpClientModule,FormsModule
  ],

  providers: [EtudiantService,ProfesseurService],
  bootstrap: [AppComponent]
})

export class AppModule { }
