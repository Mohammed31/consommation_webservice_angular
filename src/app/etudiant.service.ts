import { Injectable } from '@angular/core';
import {HttpHeaders,HttpClient} from '@angular/common/http';
import { Etudiant } from '../etudiant';
import { Observable, of } from 'rxjs';
import 'rxjs/add/operator/map'; 
import {catchError,map,tap} from 'rxjs/operators'



const httpOptions={
  headers: new HttpHeaders({ 'Content-Type':'application/JSON' })
};

@Injectable()

export class EtudiantService {

  private APIUrl = 'http://localhost:8080/etudiants/delete';
  private APIUrlsave = 'http://localhost:8080/etudiants/save';

  constructor(public http: HttpClient) { }
    
    // getlistEtudiants():Observable<Etudiant[]>{
    //   return this.http.get<Etudiant[]>(this.etudiantUrl)
    //   .pipe(
    //     tap(etudiants=>this.log('etudiant recuperer')),
    //     catchError(this.handleError('getlistEtudiants',[]))
    //   );
    // }
private handleError<T> (operation = 'operation', result?: T) {
  return (error: any): Observable<T> => {
    console.error(error); 
    this.log('hela');
    return of(result as T);
  };
}

private log(mesg:string) {
  console.log("ca marche ");
}

getlistEtudiants(){
  return this.http.get("http://localhost:8080/etudiants/list")
  .map(resp => resp);
 // un exemple pour me reperer  return this.http.get("/api/MediaItem").map(response => { response.json() });
  // .pipe(
  //        tap(etudiants=>this.log('etudiant recuperer')),
  //        catchError(this.handleError('getlistEtudiants',[]))
  //      );  
}

// essai 1 sans succes 
// recherche des etudiants 
// rechercheEtudiant(idrechercher:number):Observable<Etudiant>{
//   const url=`${}/?id=`
//   return this.http.get("http://localhost:8080/etudiants/"+this.idrechercher)
//   .map(reponse=>reponse);
// }

// essai 2 sans succes 
// rechercheEtudiant<Data>(id: number): Observable<Etudiant> {
//   const url = `${this.APIUrl}/{find/id=1}`;
//   return this.http.get<Etudiant>(url)
//     .pipe(
//       map(listestudiants => listestudiants), // returns a {0|1} element array
//       tap(h => {
//         const outcome = h ? `fetched` : `did not find`;
//         this.log(`${outcome} Etudiant id=${id}`);
//       }),
//       catchError(this.handleError<Etudiant>("pas trouver"))
//     );
// }

public rechercheEtudiant(id:number):Observable<Etudiant>{
  console.log("je suis dans service  "+id);
  return this.getlistEtudiants().pipe(
    map((listetudiants:Etudiant[]) => listetudiants.find(etu=>etu.id===id))
  );
  
}

public deletEtud(etud:Etudiant | number): Observable<Etudiant>{

  const id = typeof etud === 'number'? etud : etud.id;
  const url = `${this.APIUrl}/${id}`;

  return this.http.delete<Etudiant>(url,httpOptions).pipe(
    tap(_=>this.log('delete and gone')),
    catchError(this.handleError<Etudiant>('deletEtud'))
  );
}

ajouteretudiant (etudiant:Etudiant):Observable<Etudiant>{
  return this.http.post<Etudiant>(this.APIUrlsave,etudiant,httpOptions).pipe(
  tap((etudiant:Etudiant) => this.log(`ajouter etudiant w/ id=${etudiant.id}`)),
    catchError(this.handleError<Etudiant>('ajouteretudiant'))
  );
}



}

