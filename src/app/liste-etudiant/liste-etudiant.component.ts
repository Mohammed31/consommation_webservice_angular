import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import{Http} from '@angular/http';
import { Observable, Subject, pipe,of } from 'rxjs';
import 'rxjs/add/operator/map'; 
import 'rxjs/add/operator/catch';
import {EtudiantService} from'../etudiant.service';
import { Etudiant } from '../../etudiant';


@Component({
  selector: 'app-liste-etudiant',
  templateUrl: './liste-etudiant.component.html',
  styleUrls: ['./liste-etudiant.component.css']
})
export class ListeEtudiantComponent implements OnInit {

pagelistEtudiant:any;
particlesJS: any;
// fackedata={nom:"kada",prenom:"jeloa"};

  constructor(public http:Http,public etudiantservice:EtudiantService ) { }

  ngOnInit() {
this.etudiantservice.getlistEtudiants()
.subscribe(data=>{
     this.pagelistEtudiant=data;
   },error=>{
   console.log("error");
   })
//     this.http.get("http://localhost:8080/etudiants/list")
// .map(resp=>resp.json()
// .subscribe(data=>{
//   this.pagelistEtudiant=data;
// },error=>{
// console.log("error");
// })
  }
  // getlistEtudiants():void {
  //   this.etudiantservice.getlistEtudiants()
  //   .subscribe(pagelistEtudiant=>this.pagelistEtudiant=pagelistEtudiant);
  // }

  public deletEtud(etud:Etudiant):void{
    this.pagelistEtudiant = this.pagelistEtudiant.filter(e=>e !==etud);
    this.etudiantservice.deletEtud(etud).subscribe();
  }
}
