import { TestBed, inject } from '@angular/core/testing';

import { ProfesseurService } from './professeur.service';

describe('ProfesseurService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProfesseurService]
    });
  });

  it('should be created', inject([ProfesseurService], (service: ProfesseurService) => {
    expect(service).toBeTruthy();
  }));
});
